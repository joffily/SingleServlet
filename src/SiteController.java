import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SiteController {

	
	public RequestDispatcher index(HttpServletRequest request, HttpServletResponse response) {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/view/site/index.jsp");
		return dispatcher;
	}

	public RequestDispatcher cadastro(HttpServletRequest request, HttpServletResponse response) {
		// Podemos receber por meio de qualquer método HTTP
		// Podemos adicionar a logica de negócio aqui ou abstrair em outras classes
		// Podemos adicionar filtros (todo)
		// TODO: Testar Flash messages flash[:alert]
		// TODO: Aplicar filtros de verbos HTTP
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/view/site/cadastro.jsp");
		return dispatcher;
	}
}
