# Single Controller

Buscando uma mandeira de implementar Servlets de maneira mais econômica, nós alunos da disciplina de PWEB II resolvemos elaborar um modelo de aplicação orientada ao path de acesso do usuário.

Funciona da seguinte maneira, ao acessar uma página em nossa aplicação como por exemplo (http://localhost:8080/single/site/index) o SingleServlet tentará mapear o /site/ para um controller chamado SiteController e executará o método **index** desta classe. É esperado que todo método responda com **RequestDispatcher** e então o SingleServlet se encarregará de fazer o forward para a view requisitada.


## TODO

- É preciso melhorar o método que filtra as URLs (Regex talvez?)
- É preciso testar com flash messages
- É preciso implementar um caso de uso parecido com o apresentado em sala de aula


